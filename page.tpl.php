<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <?php print $ie_styles; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
</head>
<body class="<?php print $body_classes; ?>">
  <div id="page" class="container_16">
    <div id="header" class="grid_16 alpha omega">
      <div id="logo-title" class="grid_16 alpha omega">

        <?php if (!empty($logo)): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
        <?php endif; ?>

        <div id="name-and-slogan">
          <?php if (!empty($site_name)): ?>
            <h1 id="site-name">
              <a href="<?php print $front_page ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </h1>
          <?php endif; ?>

          <?php if (!empty($site_slogan)): ?>
            <div id="site-slogan"><?php print $site_slogan; ?></div>
          <?php endif; ?>
        </div> <!-- /name-and-slogan -->
      </div> <!-- /logo-title -->

      <?php if (!empty($header)): ?>
        <div id="header-region" class="<?php if (!empty($search_box)) { print 'grid_8 '; }else{ print 'grid_11 '; } ?>omega">
          <?php print $header; ?>
        </div>
      <?php endif; ?>
      <div id="navigation" class="grid_16 alpha omega <?php if (!empty($primary_links)) { print "withprimary"; } ?> clearfix">
        <?php if (!empty($primary_links)): ?>
          <div id="sf-menu" >
            <?php print $primary_links_tree; ?>
          </div>
        <?php endif; ?>
      </div> <!-- /navigation -->
    </div> <!-- /header -->
       <?php if ($breadcrumb): ?>
      <div id="breadcrumb" class="grid_16"><?php print $breadcrumb; ?></div>
    <?php endif; ?>
    <div id="container" class="grid_16 alpha omega">
     <?php if ((!empty($left)) && (!empty($right))): ?>      
      <div id="main" class="clearfix grid_9 alpha">
     <?php elseif (!empty($left)): ?>        
      <div id="main" class="clearfix grid_12 alpha">
      <?php elseif (!empty($right)): ?>
      <div id="main" class="clearfix grid_12 alpha"> 
       <?php else: ?>       
      <div id="main" class="clearfix"> 
      <?php endif; ?>      
    
	<div id="main-squeeze">
	  	<?php if(!$is_front): ?>
	  	<div class="shadow-container">
			<div class="shadow1">
				<div class="shadow2">
					<div class="shadow3">
		<?php endif; ?>
        			<div id="content">
          			<?php if (!empty($title)): ?><h2 class="title" id="page-title"><?php print $title; ?></h2><?php endif; ?>
          			<?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
          			<?php if (!empty($messages)): print $messages; endif; ?>
          			<?php if (!empty($help)): print $help; endif; ?>
          				<div id="content-content" >
           		 		<?php print $content; ?>
          				</div> <!-- /content-content -->
          				<?php print $feed_icons; ?>
        			</div> <!-- /content -->
        <?php if(!$is_front): ?>
        			      </div> <!-- /shadow3 -->
      			  </div><!-- /shadow2 -->
			  </div><!-- /shadow1-->
	 	  </div><!-- /shadow-container -->
		<?php endif; ?>

      				</div>      
</div>

      <?php if (!empty($left)): ?>
      	<?php if (!empty($right)): ?>
        <div id="sidebar-left" class="grid_3">
        <?php else: ?>        
        <div id="sidebar-left" class="grid_3 omega">
        <?php endif; ?>
          <?php print $left; ?>
        </div> <!-- /sidebar-left -->
      <?php endif; ?>

      <?php if (!empty($right)): ?>
        <div id="sidebar-right" class="grid_3 omega">
          <?php print $right; ?>
        </div> <!-- /sidebar-right -->
      <?php endif; ?>

    </div> <!-- /container -->
  </div> <!-- /page -->
  
    <div id="footer-wrapper">
      <div id="footer" class="container_16">
        <?php print $footer_message; ?>
        <?php if (!empty($footer)): print $footer; endif; ?>
      </div> <!-- /footer -->
    </div> <!-- /footer-wrapper -->

    <?php print $closure; ?>


</body>
</html>
