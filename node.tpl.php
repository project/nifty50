<?php
?>
	  	<?php if($is_front): ?>
	  	<div class="shadow-container">
			<div class="shadow1">
				<div class="shadow2">
					<div class="shadow3">
		<?php endif; ?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> clear-block">

<?php print $picture ?>

<?php if (!$page): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <div class="meta">
  <?php if ($submitted): ?>
    <span class="submitted"><?php print $submitted ?></span>
  <?php endif; ?>

  <?php if ($terms): ?>
    <div class="terms terms-inline"><?php print $terms ?></div>
  <?php endif;?>
  </div>

  <div class="content">
    <?php print $content ?>
  </div>

  <?php print $links; ?>
</div>
			        <?php if($is_front): ?>
        			      </div> <!-- /shadow3 -->
      			  </div><!-- /shadow2 -->
			  </div><!-- /shadow1-->
	 	  </div><!-- /shadow-container -->
		<?php endif; ?>